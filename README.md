# 🐳 `docker` "devbox" 📦 

```
 ⚠️️ Disclaimer: This is working in progress and should only be used as a template for a devbox when you know what you are doing. It has only been tested on OS X.
```

## Basic Usage

1. Prepare your machine. This will setup a looback device for the devbox IP `172.16.200.1` and the DNS resolver for `devbox.test`.

    ```shell
    $ sudo ./script/prepare_machine
    ```

1. Define Services in `docker-compose.yml` using `nginx/proxy`'s `VIRTUAL_HOST` and `VIRTUAL_PORT` to expose the services under subdomains of `devbox.test`.

    ```yml
        myservice:
            image: nginx
            environment:
                VIRTUAL_HOST: myservice.devbox.test
                VIRTUAL_PORT: 8080
    ```

1. Configure a development setup for a service by adding a directory containing a `docker-compose` file to `./development` and linking the local checkout of the service's code to `./mount/<service name>/`.

1. Setup the environment by sourcing `./script/setup_environment`.

    ```shell
    $ . ./script/setup_environment
    ```

1. Now you can use `docker-compose` to start the devbox and start developing

    ```shell
    $ docker-compose up -d
    ```